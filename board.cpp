#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

bool Board::isSide(Side side, int i, int j) {
	return (get(side, i, j) &&
		       ((i == 0 && j > 1 && j < 6) ||
		        (i == 7 && j > 1 && j < 6) ||
		        (j == 0 && i > 1 && i < 6) || 
		        (j == 7 && i > 1 && i < 6)));
}

bool Board::isCorner(Side side, int i, int j) {
	return (get(side, i, j) &&
	            ((i == 0 and j == 0) || 
                 (i == 0 and j == 7) || 
                 (i == 7 and j == 0) || 
                 (i == 7 and j == 7)));
}

bool Board::isEdgeAdj(Side side, int i, int j) {
	return get(side, i, j) == side &&
		       ((i == 1 && j > 1 && j < 6) ||
		        (i == 6 && j > 1 && j < 6) ||
		        (j == 1 && i > 1 && i < 6) || 
		        (j == 6 && i > 1 && i < 6));
}

bool Board::isCornerAdj(Side side, int i, int j) {
	return get(side, i, j) && 
              ((i == 0 && j == 1) || (i == 1 && j == 0) || (i == 1 && j == 1) ||
               (i == 0 && j == 6) || (i == 1 && j == 6) || (i == 1 && j == 7) ||
               (i == 6 && j == 0) || (i == 6 && j == 1) || (i == 7 && j == 1) ||
               (i == 6 && j == 6) || (i == 6 && j == 7) || (i == 7 && j == 6));
}

bool Board::isStable(Side side, int i, int j) {

    // Corners are stable
    if(isCorner(side, i, j))
    {
        return true;
    }

    // Diagonally adj to corner and we have corner
    if((get(side, i, j) && (i == 1 && j == 1) && get(side, 0, 0)) ||
        (get(side, i, j) && (i == 1 && j == 6) && get(side, 0, 7)) ||
        (get(side, i, j) && (i == 6 && j == 1) && get(side, 7, 0)) ||
        (get(side, i, j) && (i == 6 && j == 6) && get(side, 7, 7)))
    {
        return true;
    }

    if(isSide(side, i, j))
    {
        bool stable = true;

        if(i == 0 || i == 7)
        {
            for(int j0 = j; j0 >= 0 && stable; j0--)
            {
                if(!get(side, i, j0))
                {
                    stable = false;
                }
            }

            if(stable)
            {
                return true;
            }

            else
            {
                stable = true;
                for(int j0 = j; j0 <= 7 && stable; j0++)
                {
                    if(!get(side, i, j0))
                    {
                        stable = false;
                    }
                }

                if(stable)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        else
        {
            for(int i0 = i; i0 >= 0 && stable; i0--)
            {
                if(!get(side, i0, j))
                {
                    stable = false;
                }
            }

            if(stable)
            {
                return true;
            }

            else
            {
                stable = true;
                for(int i0 = i; i0 <= 7 && stable; i0++)
                {
                    if(!get(side, i0, j))
                    {
                        stable = false;
                    }
                }

                if(stable)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    return false; 
}

int Board::boardScore(Side side) {
	int total = count(side);
     for(int i = 0; i < 8; i++)
     {
        for(int j = 0; j < 8; j++)
        {
		   // add 100 points for friendly corner pieces
		   if(isStable(side, i, j))
		   {
			   total += 100;
		   }

           // add -30 points for pieces adjacent to corners
           else if(isCornerAdj(side, i, j))
           {
            total -= 30;
           }
		  
		   // add 10 points for friendly edge pieces
		   if(isSide(side, i, j))
             {
			   total += 10;
		     }

		   // add -2 points for pieces adjacent to edges
		   if(isEdgeAdj(side, i, j))
            {
			   total -= 2;
		    }
	   }
   }

     return total;
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
