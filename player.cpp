#include "player.h"
using namespace std;

// ROHAN'S EDIT

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     * 
     * Adding a comment here.
     * 
     */
     
     this->side = side;
     other = (side == BLACK) ? WHITE : BLACK;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     Othello.doMove(opponentsMove, other);
     map<Move*, int> moves;
     Move * final_move;
     Board Othello_test;
     int score = 0;

     for(int i = 0; i < 8; i++)
     {
        for(int j = 0; j < 8; j++)
        {
            Move *m = new Move(i, j);

            if(Othello.checkMove(m, side))
            {
                Othello_test = Othello;
                Othello_test.doMove(m, side);
                moves[m] = doOtherMove(Othello_test);
                std::cerr << "AI " << m->getX() << ", " << m->getY() << ": " << moves[m] <<std::endl;

            }
            
            else
            {
                delete m;
		    }
        }
     }

     if(moves.size() > 0)
     { 
		// Find the square that has the highest heuristic score
        map<Move*, int>::iterator iter;
        score = moves.begin()->second;
        final_move = moves.begin()->first;
		for(iter = moves.begin(); iter != moves.end(); iter++)
		{
			if(iter->second > score)
            {
                final_move = iter->first;
                score = iter->second;
            }
		}

        Othello.doMove(final_move, side);
        return final_move;
     }

     else
     {
        return NULL;
     }
}

int Player::doOtherMove(Board Othello_test) {
    
     map<Move*, int> otherMoves;
     Board Othello_test2;
     int score = 0;

     for(int i = 0; i < 8; i++)
     {
        for(int j = 0; j < 8; j++)
        {
            Move *m = new Move(i, j);

            if(Othello_test.checkMove(m, other))
            {
                Othello_test2 = Othello_test;
                Othello_test2.doMove(m, other);
                score = Othello_test2.boardScore(side) - Othello_test2.boardScore(other);
                otherMoves[m] = score;
                std::cerr << "Other " << m->getX() << ", " << m->getY() << ": " << otherMoves[m] <<std::endl;

            }
            
            else
            {
                delete m;
		    }
        }
     }

     if(otherMoves.size() > 0)
     { 
		// Find the square that has the minimum heuristic score
        map<Move*, int>::iterator iter;
        score = otherMoves.begin()->second;
		for(iter = otherMoves.begin(); iter != otherMoves.end(); iter++)
		{
			if(iter->second < score)
            {
                score = iter->second;
            }
		}
        return score;
     }

     else
     {
        return 0;
     }
}
