#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <map>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int doOtherMove(Board Othello_test);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    Board Othello;
    Board Stable;
    Side other;
    Side side;
};

#endif
